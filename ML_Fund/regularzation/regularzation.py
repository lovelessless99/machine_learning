import numpy as np
import matplotlib.pyplot as plt


def to_matrix(x):
    return np.vstack([
        np.ones(x.size),
        x,
        x ** 2,
        x ** 3,
        x ** 4,
        x ** 5,
        x ** 6,
        x ** 7,
        x ** 8,
        x ** 9,
        x ** 10
    ]).T


def polyregression(errors, train_x, train_y, regular = False):

    eta = 1e-4
    diff = 1
    theta = np.random.rand(train_x.shape[1])

    errors.append(MSE(train_x, train_y, theta))

    while diff > 1e-6:
        reg = 0.5 * np.hstack([0, theta[1:]])
        if regular is False:
            theta = theta - eta * (np.dot(f(train_x, theta) - train_y, train_x))
        else:
            theta = theta - eta * (np.dot(f(train_x, theta) - train_y, train_x) + reg)

        errors.append(MSE(train_x, train_y, theta))
        diff = errors[-2] - errors[-1]

    return theta


def MSE(x, y, theta):
    return 0.5 * np.sum((y-f(x, theta))**2)


def f(x, theta):
    return np.dot(x, theta)


if __name__ == '__main__':
    g = lambda x: 0.1 * (x ** 3 + x ** 2 + x)

    train_x = np.linspace(-2, 2, 8)
    std_train_x = (train_x - train_x.mean())/train_x.std()
    z = to_matrix(std_train_x)

    train_y = g(train_x) + np.random.randn(train_x.size)*0.05

    errors = []
    errors_reg = []

    theta = polyregression(errors, z, train_y, False)
    theta1 = polyregression(errors_reg, z, train_y, True)

    plt.plot(std_train_x, train_y, 'o')
    train_x1 = np.linspace(-2, 2, 100)
    std_train_x = (train_x1 - train_x.mean())/train_x.std()
    z = to_matrix(std_train_x)
    plt.plot(std_train_x, f(z, theta))
    plt.plot(std_train_x, f(z, theta1), '--g')
    plt.show()
