import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def normalize(train_x):
    mu = train_x.mean()
    sigma = train_x.std()
    return (train_x-mu)/sigma


def f(x, theta):
    return np.dot(x, theta)


def MSE(x, y, theta):
    return (1 / x.shape[0]) * np.sum((y-f(x, theta))**2)


def polyregression(errors, train_x, train_y):

    eta = 1e-3
    diff = 1
    theta = np.random.rand(3)

    errors.append(MSE(train_x, train_y, theta))

    while diff > 1e-2:
        theta = theta - eta * np.dot(f(train_x, theta) - train_y, train_x)

        errors.append(MSE(train_x, train_y, theta))
        diff = errors[-2] - errors[-1]

    return theta


def SGD(errors, train_x, train_y):
    eta = 1e-3
    diff = 1
    theta = np.random.rand(3)

    errors.append(MSE(train_x, train_y, theta))

    while diff > 1e-2:
        p = np.random.permutation(train_x.shape[0])
        for x, y in zip(train_x[p, :], train_y[p]):
            theta = theta - eta * (f(x, theta) - y) * x

        errors.append(MSE(train_x, train_y, theta))
        diff = errors[-2] - errors[-1]

    return theta


def plot_result(learner, errors):
    train = pd.read_csv('click.csv', sep=',')
    train_x = normalize(train.iloc[:, 0])
    train_y = train.iloc[:, 1]

    X = np.vstack([np.ones(train_x.shape[0]), train_x, train_x ** 2]).T

    x = np.linspace(-3, 3, 100)
    x = np.vstack([np.ones(x.shape[0]), x, x ** 2]).T

    plt.plot(train_x, train_y, 'o')
    plt.plot(np.linspace(-3, 3, 100), f(x, learner(errors, X, train_y)))
    plt.show()

    plt.plot(np.arange(len(errors)), errors)
    plt.show()


if __name__ == '__main__':
    errors_poly = []
    errors_SGD = []
    plot_result(polyregression, errors_poly)
    plot_result(SGD, errors_SGD)
