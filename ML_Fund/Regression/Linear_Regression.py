import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def plot_data(train_x, train_y):
    plt.plot(train_x, train_y, 'o')
    #plt.show()


def normalize(train_x):
    mu = train_x.mean()
    sigma = train_x.std()
    return (train_x-mu)/sigma


def f(x, theta0, theta1):
    return theta0 + theta1 * x


def LSE(x, y, theta0,theta1):
    return 0.5 * np.sum((y-f(x, theta0, theta1))**2)


def learning(train_x, train_y):
    theta0 = np.random.rand()
    theta1 = np.random.rand()

    eta = 1e-3
    diff = 1
    count = 0
    error = LSE(train_x, train_y, theta0, theta1)
    print(error)
    while diff > 1e-2:
        tmp0 = theta0 - eta * np.sum((f(train_x, theta0, theta1) - train_y))
        tmp1 = theta1 - eta * np.sum((f(train_x, theta0, theta1) - train_y)* train_x)

        theta0 = tmp0
        theta1 = tmp1

        current_error = LSE(train_x, train_y, theta0, theta1)
        diff = error - current_error
        error = current_error

        count += 1

        log = '{} 次 : theta0 = {:.3f}, theta1 = {:.3f}, 差分 = {:.4f}'
        print(log.format(count, theta0, theta1, diff))
    return theta0, theta1


if __name__ == '__main__':
    train = pd.read_csv('click.csv', sep=',')
    train_x = normalize(train.iloc[:, 0])
    train_y = train.iloc[:, 1]
    plot_data(train_x, train_y)
    theta0, theta1 = learning(train_x, train_y)
    x = np.linspace(-3, 3, 100)
    plt.plot(x, theta0 + x*theta1)
    plt.show()