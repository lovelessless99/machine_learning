import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def perceptron(traindata, w, epoch):
    count = 0
    f = lambda x: 1 if np.dot(w, x) >= 0 else -1
    for _ in range(epoch):
        for index, row in traindata.iterrows():
            x = np.array([row[0], row[1]])
            y = row[2]
            if f(x) != y:
                w = w + x * y
                count += 1
                print('{}次: w = {}'.format(count, w))
    return w


def plot_result(train_x, train_y, w):
    x1 = np.arange(0, 500)
    plt.plot(train_x[train_y == 1]['x1'], train_x[train_y == 1]['x2'], 'o')
    plt.plot(train_x[train_y == -1]['x1'], train_x[train_y == -1]['x2'], 'x')
    plt.plot(x1, -w[0]/w[1]*x1, '--')
    plt.show()


if __name__ == '__main__':
    train = pd.read_csv('images1.csv', sep=',')
    train_x = train.iloc[:, 0:2]
    train_y = train.iloc[:, 2]
    w = perceptron(train, np.random.rand(2), 10)
    plot_result(train_x, train_y, w)