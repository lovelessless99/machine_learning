import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def normalize(train_x):
    mu = train_x.mean(axis=0)
    sigma = train_x.std(axis=0)
    return (train_x-mu)/sigma


def classify(x, theta):
    f = lambda x: 1 / (1 + np.exp(-np.dot(x, theta)))
    return (f(x) >= 0.5).astype(np.int)


def logistic_regression(train_x, train_y, epoch, eta):
    theta = np.random.rand(4)
    f = lambda x: 1 / (1 + np.exp(-np.dot(x, theta)))

    for _ in range(epoch):
        theta = theta - eta * np.dot(f(train_x) - train_y, train_x)
        result = classify(train_x, theta) == train_y
        accuracy = len(result[result == True]) / len(result)
        acc.append(accuracy)
    return theta


def SGD_logistic(train_x, train_y, epoch, eta, acc):
    theta = np.random.rand(4)
    f = lambda x: 1 / (1 + np.exp(-np.dot(x, theta)))
    for _ in range(epoch):
        p = np.random.permutation(train_x.shape[0])
        for x, y in zip(train_x[p, :], train_y[p]):
            theta = theta - eta * (f(x) - y) * x
            result = classify(train_x, theta) == train_y
            accuracy = len(result[result == True]) / len(result)
            acc.append(accuracy)

    return theta


def plot_result(theta):
    x1 = np.linspace(-5, 5, 1000)
    x2 = -(theta[0] + theta[1] * x1 + theta[3] * x1 ** 2) / theta[2]
    plt.plot(x1, x2, '--')
    plt.show()


if __name__ == '__main__':
    train = pd.read_csv('data3.csv', sep=',')
    train_x = train.iloc[:, 0:2]
    train_y = train.iloc[:, 2]
    plt.plot(train[train_y == 1]['x1'], train[train_y == 1]['x2'], 'o')
    plt.plot(train[train_y == 0]['x1'], train[train_y == 0]['x2'], 'x')

    train_x3 = np.array(train.iloc[:, 0]**2)[:, np.newaxis]
    train_x = np.hstack([np.ones([train_x.shape[0], 1]), train_x, train_x3])

    acc = []
    theta = logistic_regression(train_x, train_y, 5000, 1e-3)

    #theta = SGD_logistic(train_x, train_y, 500, 1e-3, acc)

    plot_result(theta)

    plt.plot(np.arange(len(acc)), acc)
    plt.show()


