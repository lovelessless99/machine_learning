import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def normalize(train_x):
    mu = train_x.mean()
    sigma = train_x.std()
    return (train_x-mu)/sigma


def logistic_regression(train_x, train_y, epoch, eta):
    theta = np.random.rand(3)
    X = np.hstack([np.ones([train_x.shape[0], 1]), train_x])
    f = lambda x: 1 / (1 + np.exp(-np.dot(x, theta)))

    for _ in range(epoch):
        theta = theta - eta * np.dot(f(X) - train_y, X)
    return theta


def plot_result(train_x, train_y, theta):
    x0 = np.linspace(-2, 2, 100)
    plt.plot(train_x[train_y == 1]['x1'], train_x[train_y == 1]['x2'], 'o')
    plt.plot(train_x[train_y == 0]['x1'], train_x[train_y == 0]['x2'], 'x')
    plt.plot(x0, -(theta[0] + theta[1]*x0) / theta[2], '--')
    plt.show()


if __name__ == '__main__':
    train = pd.read_csv('images2.csv', sep=',')
    train_x = normalize(train.iloc[:, 0:2])
    train_y = train.iloc[:, 2]
    theta = logistic_regression(train_x, train_y, 5000, 1e-3)
    plot_result(train_x, train_y, theta)